/**
 * Copyright (c) 2013  Forty Amp Software <http://www.40amps.com>
 *
 *  This file is part of the Spring-Mandrill-Webhooks project:
 * (https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/overview)
 *
 *  Spring-Mandrill-Webhooks is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Spring-Mandrill-Webhooks is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with Spring-Mandrill-Webhooks.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.fortyampsoftware.springmandrillwebhook.model.mandrill;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * A Mandrill SmtpEvent model object used to contain Mandrill Webhook data.
 * 
 * See <a href="http://help.mandrill.com/entries/24466132-Webhook-Format" target="_new">http://help.mandrill.com/entries/24466132-Webhook-Format</a> for details.
 *
 * @author D. Scott Felblinger
 */
public class SmtpEvent implements Serializable
{

	private static final long serialVersionUID = -2462535606525301016L;
	@JsonProperty("destination_ip")
	private String destinationIp;
	private String diag;
	
	@JsonProperty("source_ip")
	private String sourceIp;
	
	@JsonProperty("ts")
	private Long timestamp;
	
	private String type;
	
	private Long size;

	/**
	 * @return the destinationIp
	 */
	public String getDestinationIp()
	{
		return destinationIp;
	}

	/**
	 * @param destinationIp the destinationIp to set
	 */
	public void setDestinationIp(String destinationIp)
	{
		this.destinationIp = destinationIp;
	}

	/**
	 * @return the diag
	 */
	public String getDiag()
	{
		return diag;
	}

	/**
	 * @param diag the diag to set
	 */
	public void setDiag(String diag)
	{
		this.diag = diag;
	}

	/**
	 * @return the sourceIp
	 */
	public String getSourceIp()
	{
		return sourceIp;
	}

	/**
	 * @param sourceIp the sourceIp to set
	 */
	public void setSourceIp(String sourceIp)
	{
		this.sourceIp = sourceIp;
	}

	/**
	 * @return the timestamp
	 */
	public Long getTimestamp()
	{
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Long timestamp)
	{
		this.timestamp = timestamp;
	}

	/**
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type)
	{
		this.type = type;
	}

	/**
	 * @return the size
	 */
	public Long getSize()
	{
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(Long size)
	{
		this.size = size;
	}
	
	
}
