/**
 * Copyright (c) 2013  Forty Amp Software <http://www.40amps.com>
 *
 *  This file is part of the Spring-Mandrill-Webhooks project:
 * (https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/overview)
 *
 *  Spring-Mandrill-Webhooks is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Spring-Mandrill-Webhooks is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with Spring-Mandrill-Webhooks.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.fortyampsoftware.springmandrillwebhook.model.mandrill;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * A Mandrill UserAgent model object used to contain Mandrill Webhook data.
 * 
 * See <a href="http://help.mandrill.com/entries/24466132-Webhook-Format" target="_new">http://help.mandrill.com/entries/24466132-Webhook-Format</a> for details.
 *
 * @author D. Scott Felblinger
 */
public class UserAgent implements Serializable
{

	private static final long serialVersionUID = 8713338498329849407L;

	private Boolean mobile;
	
	@JsonProperty("os_company")
	private String osCompany;
	
	@JsonProperty("os_company_url")
	private String osCompanyUrl;
	
	@JsonProperty("os_family")
	private String osFamily;
	
	@JsonProperty("os_icon")
	private String osIcon;
	
	@JsonProperty("os_name")
	private String osName;
	
	@JsonProperty("os_url")
	private String osUrl;
	
	private String type;
	
	@JsonProperty("ua_company")
	private String uaCompany;
	
	@JsonProperty("ua_company_url")
	private String uaCompanyUrl;
	
	@JsonProperty("ua_family")
	private String uaFamily;
	
	@JsonProperty("ua_icon")
	private String uaIcon;
	
	@JsonProperty("ua_name")
	private String uaName;
	
	@JsonProperty("ua_url")
	private String uaUrl;
	
	@JsonProperty("ua_version")
	private String uaVersion;

	/**
	 * @return the mobile
	 */
	public Boolean getMobile()
	{
		return mobile;
	}

	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(Boolean mobile)
	{
		this.mobile = mobile;
	}

	/**
	 * @return the osCompany
	 */
	public String getOsCompany()
	{
		return osCompany;
	}

	/**
	 * @param osCompany the osCompany to set
	 */
	public void setOsCompany(String osCompany)
	{
		this.osCompany = osCompany;
	}

	/**
	 * @return the osCompanyUrl
	 */
	public String getOsCompanyUrl()
	{
		return osCompanyUrl;
	}

	/**
	 * @param osCompanyUrl the osCompanyUrl to set
	 */
	public void setOsCompanyUrl(String osCompanyUrl)
	{
		this.osCompanyUrl = osCompanyUrl;
	}

	/**
	 * @return the osFamily
	 */
	public String getOsFamily()
	{
		return osFamily;
	}

	/**
	 * @param osFamily the osFamily to set
	 */
	public void setOsFamily(String osFamily)
	{
		this.osFamily = osFamily;
	}

	/**
	 * @return the osIcon
	 */
	public String getOsIcon()
	{
		return osIcon;
	}

	/**
	 * @param osIcon the osIcon to set
	 */
	public void setOsIcon(String osIcon)
	{
		this.osIcon = osIcon;
	}

	/**
	 * @return the osName
	 */
	public String getOsName()
	{
		return osName;
	}

	/**
	 * @param osName the osName to set
	 */
	public void setOsName(String osName)
	{
		this.osName = osName;
	}

	/**
	 * @return the osUrl
	 */
	public String getOsUrl()
	{
		return osUrl;
	}

	/**
	 * @param osUrl the osUrl to set
	 */
	public void setOsUrl(String osUrl)
	{
		this.osUrl = osUrl;
	}

	/**
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type)
	{
		this.type = type;
	}

	/**
	 * @return the uaCompany
	 */
	public String getUaCompany()
	{
		return uaCompany;
	}

	/**
	 * @param uaCompany the uaCompany to set
	 */
	public void setUaCompany(String uaCompany)
	{
		this.uaCompany = uaCompany;
	}

	/**
	 * @return the uaCompanyUrl
	 */
	public String getUaCompanyUrl()
	{
		return uaCompanyUrl;
	}

	/**
	 * @param uaCompanyUrl the uaCompanyUrl to set
	 */
	public void setUaCompanyUrl(String uaCompanyUrl)
	{
		this.uaCompanyUrl = uaCompanyUrl;
	}

	/**
	 * @return the uaFamily
	 */
	public String getUaFamily()
	{
		return uaFamily;
	}

	/**
	 * @param uaFamily the uaFamily to set
	 */
	public void setUaFamily(String uaFamily)
	{
		this.uaFamily = uaFamily;
	}

	/**
	 * @return the uaIcon
	 */
	public String getUaIcon()
	{
		return uaIcon;
	}

	/**
	 * @param uaIcon the uaIcon to set
	 */
	public void setUaIcon(String uaIcon)
	{
		this.uaIcon = uaIcon;
	}

	/**
	 * @return the uaName
	 */
	public String getUaName()
	{
		return uaName;
	}

	/**
	 * @param uaName the uaName to set
	 */
	public void setUaName(String uaName)
	{
		this.uaName = uaName;
	}

	/**
	 * @return the uaUrl
	 */
	public String getUaUrl()
	{
		return uaUrl;
	}

	/**
	 * @param uaUrl the uaUrl to set
	 */
	public void setUaUrl(String uaUrl)
	{
		this.uaUrl = uaUrl;
	}

	/**
	 * @return the uaVersion
	 */
	public String getUaVersion()
	{
		return uaVersion;
	}

	/**
	 * @param uaVersion the uaVersion to set
	 */
	public void setUaVersion(String uaVersion)
	{
		this.uaVersion = uaVersion;
	}
	
	
}
