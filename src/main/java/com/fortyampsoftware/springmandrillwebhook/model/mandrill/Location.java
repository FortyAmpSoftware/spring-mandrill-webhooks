/**
 * Copyright (c) 2013  Forty Amp Software <http://www.40amps.com>
 *
 *  This file is part of the Spring-Mandrill-Webhooks project:
 * (https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/overview)
 *
 *  Spring-Mandrill-Webhooks is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Spring-Mandrill-Webhooks is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with Spring-Mandrill-Webhooks.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.fortyampsoftware.springmandrillwebhook.model.mandrill;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * A Mandrill Location model object used to contain Mandrill Webhook data.
 * 
 * See <a href="http://help.mandrill.com/entries/24466132-Webhook-Format" target="_new">http://help.mandrill.com/entries/24466132-Webhook-Format</a> for details.
 *
 * @author D. Scott Felblinger
 */
public class Location implements Serializable
{

	private static final long serialVersionUID = -4462605442524343677L;

	@JsonProperty("country_short")
	private String countryShort;
	
	private String country;
	private String region;
	private String city;
	
	@JsonProperty("postal_code")
	private String postalCode;
	private String timezone;
	private Float latitude;
	private Float longitude;
	/**
	 * @return the countryShort
	 */
	public String getCountryShort()
	{
		return countryShort;
	}
	/**
	 * @param countryShort the countryShort to set
	 */
	public void setCountryShort(String countryShort)
	{
		this.countryShort = countryShort;
	}
	/**
	 * @return the country
	 */
	public String getCountry()
	{
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country)
	{
		this.country = country;
	}
	/**
	 * @return the region
	 */
	public String getRegion()
	{
		return region;
	}
	/**
	 * @param region the region to set
	 */
	public void setRegion(String region)
	{
		this.region = region;
	}
	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city)
	{
		this.city = city;
	}
	/**
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return postalCode;
	}
	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode)
	{
		this.postalCode = postalCode;
	}
	/**
	 * @return the timezone
	 */
	public String getTimezone()
	{
		return timezone;
	}
	/**
	 * @param timezone the timezone to set
	 */
	public void setTimezone(String timezone)
	{
		this.timezone = timezone;
	}
	/**
	 * @return the latitude
	 */
	public Float getLatitude()
	{
		return latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(Float latitude)
	{
		this.latitude = latitude;
	}
	/**
	 * @return the longitude
	 */
	public Float getLongitude()
	{
		return longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(Float longitude)
	{
		this.longitude = longitude;
	}
	
	
	
	
}
