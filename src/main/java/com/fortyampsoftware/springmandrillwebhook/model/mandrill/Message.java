/**
 * Copyright (c) 2013  Forty Amp Software <http://www.40amps.com>
 *
 *  This file is part of the Spring-Mandrill-Webhooks project:
 * (https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/overview)
 *
 *  Spring-Mandrill-Webhooks is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Spring-Mandrill-Webhooks is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with Spring-Mandrill-Webhooks.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.fortyampsoftware.springmandrillwebhook.model.mandrill;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A Mandrill Message (msg) model object used to contain Mandrill Webhook data.
 * 
 * See <a href="http://help.mandrill.com/entries/24466132-Webhook-Format" target="_new">http://help.mandrill.com/entries/24466132-Webhook-Format</a> for details.
 *
 * @author D. Scott Felblinger
 */
public class Message implements Serializable
{

	private static final long serialVersionUID = 522178261645651413L;
	@JsonProperty("_id")
	private String id;
	@JsonProperty("_version")
	private String version;
	private Long ts;
	private String email;
	private String sender;
	private String subject;
	
	@JsonProperty("smtp_events")
	private List<SmtpEvent> smtpEvents = new ArrayList<SmtpEvent>();
	private List<Open> opens = new ArrayList<Open>();
	
	@JsonProperty ("opens_detail")
	private List<String> opensDetail = new ArrayList<String>();
	
	private List<Click> clicks = new ArrayList<Click>();

	@JsonProperty("clicks_detail")
	private List<String>clicksDetail = new ArrayList<String>();
	private List<String> tags = new ArrayList<String>();
	
	private Map<String, String> metadata;
	private String state;
	private String subaccount;
	private List<Map<String,Object>> resends;
	private String diag;
	
	@JsonProperty("bounce_description")
	private String bounceDescription;
	private String template;

	@JsonProperty("bgtools_code")
	private String bgtoolsCode;
	
	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id)
	{
		this.id = id;
	}
	/**
	 * @return the version
	 */
	public String getVersion()
	{
		return version;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version)
	{
		this.version = version;
	}
	/**
	 * @return the ts
	 */
	public Long getTs()
	{
		return ts;
	}
	/**
	 * @param ts the ts to set
	 */
	public void setTs(Long ts)
	{
		this.ts = ts;
	}
	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}
	/**
	 * @return the sender
	 */
	public String getSender()
	{
		return sender;
	}
	/**
	 * @param sender the sender to set
	 */
	public void setSender(String sender)
	{
		this.sender = sender;
	}
	/**
	 * @return the subject
	 */
	public String getSubject()
	{
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject)
	{
		this.subject = subject;
	}
	/**
	 * @return the smtpEvents
	 */
	public List<SmtpEvent> getSmtpEvents()
	{
		return smtpEvents;
	}
	/**
	 * @param smtpEvents the smtpEvents to set
	 */
	public void setSmtpEvents(List<SmtpEvent> smtpEvents)
	{
		this.smtpEvents = smtpEvents;
	}
	/**
	 * @return the opens
	 */
	public List<Open> getOpens()
	{
		return opens;
	}
	/**
	 * @param opens the opens to set
	 */
	public void setOpens(List<Open> opens)
	{
		this.opens = opens;
	}
	
	
	
	/**
	 * @return the opensDetail
	 */
	public List<String> getOpensDetail()
	{
		return opensDetail;
	}
	/**
	 * @param opensDetail the opensDetail to set
	 */
	public void setOpensDetail(List<String> opensDetail)
	{
		this.opensDetail = opensDetail;
	}
	/**
	 * @return the clicksDetail
	 */
	public List<String> getClicksDetail()
	{
		return clicksDetail;
	}
	/**
	 * @param clicksDetail the clicksDetail to set
	 */
	public void setClicksDetail(List<String> clicksDetail)
	{
		this.clicksDetail = clicksDetail;
	}
	/**
	 * @return the clicks
	 */
	public List<Click> getClicks()
	{
		return clicks;
	}
	/**
	 * @param clicks the clicks to set
	 */
	public void setClicks(List<Click> clicks)
	{
		this.clicks = clicks;
	}
	/**
	 * @return the tags
	 */
	public List<String> getTags()
	{
		return tags;
	}
	/**
	 * @param tags the tags to set
	 */
	public void setTags(List<String> tags)
	{
		this.tags = tags;
	}

	/**
	 * @return the metadata
	 */
	public Map<String, String> getMetadata()
	{
		return metadata;
	}
	/**
	 * @param metadata the metadata to set
	 */
	public void setMetadata(Map<String, String> metadata)
	{
		this.metadata = metadata;
	}
	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state)
	{
		this.state = state;
	}
	/**
	 * @return the subaccount
	 */
	public String getSubaccount()
	{
		return subaccount;
	}
	/**
	 * @param subaccount the subaccount to set
	 */
	public void setSubaccount(String subaccount)
	{
		this.subaccount = subaccount;
	}
	/**
	 * @return the resends
	 */
	public List<Map<String,Object>> getResends()
	{
		return resends;
	}
	/**
	 * @param resends the resends to set
	 */
	public void setResends(List<Map<String,Object>> resends)
	{
		this.resends = resends;
	}
	/**
	 * @return the diag
	 */
	public String getDiag()
	{
		return diag;
	}
	/**
	 * @param diag the diag to set
	 */
	public void setDiag(String diag)
	{
		this.diag = diag;
	}
	/**
	 * @return the bounceDescription
	 */
	public String getBounceDescription()
	{
		return bounceDescription;
	}
	/**
	 * @param bounceDescription the bounceDescription to set
	 */
	public void setBounceDescription(String bounceDescription)
	{
		this.bounceDescription = bounceDescription;
	}
	/**
	 * @return the template
	 */
	public String getTemplate()
	{
		return template;
	}
	/**
	 * @param template the template to set
	 */
	public void setTemplate(String template)
	{
		this.template = template;
	}
	/**
	 * @return the bgtoolsCode
	 */
	public String getBgtoolsCode()
	{
		return bgtoolsCode;
	}
	/**
	 * @param bgtoolsCode the bgtoolsCode to set
	 */
	public void setBgtoolsCode(String bgtoolsCode)
	{
		this.bgtoolsCode = bgtoolsCode;
	}
	
	
	
}
