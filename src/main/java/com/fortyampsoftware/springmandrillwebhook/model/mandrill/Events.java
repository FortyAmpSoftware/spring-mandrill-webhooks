/**
 * Copyright (c) 2013  Forty Amp Software <http://www.40amps.com>
 *
 *  This file is part of the Spring-Mandrill-Webhooks project:
 * (https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/overview)
 *
 *  Spring-Mandrill-Webhooks is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Spring-Mandrill-Webhooks is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with Spring-Mandrill-Webhooks.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.fortyampsoftware.springmandrillwebhook.model.mandrill;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Parent container (model) object instantiated to parse JSON data received from a Mandrill
 * Webhook request.  The Events object is the parent object that contains all child objects
 * for a given Webhook request.
 * 
 * See <a href="http://help.mandrill.com/entries/24466132-Webhook-Format" target="_new">http://help.mandrill.com/entries/24466132-Webhook-Format</a> for details.
 * 
 * @author D. Scott Felblinger
 */
public class Events implements Serializable
{

	private static final long serialVersionUID = 8133646617981809803L;

	/**
	 * List containing Mandrill EventData.
	 */
	private ArrayList<EventData> events = new ArrayList<EventData>();
	
	/**
	 * The Jackson JSON Object Mapper used to map data into child objects.
	 */
	private ObjectMapper mapper = new ObjectMapper();
	
	/**
	 * The logger.  Of course.
	 */
	private static final Logger log = LoggerFactory.getLogger(Events.class);
	
	
	/**
	 * Constructor used to create Mandrill Events.  Errors parsing JSON data will result in local
	 * logging to the error stream, stack traces and a throw of the error up the stack.
	 *  
	 * @param eventData The raw JSON data to be parsed by this class into valid model objects
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public Events(String eventData) throws JsonParseException,JsonMappingException,IOException
	{
		if(eventData != null)
		{
			try
			{
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				TypeReference<ArrayList<EventData>> typeRef = new TypeReference<ArrayList<EventData>>(){};
				events = mapper.readValue(eventData, typeRef);
			} 
			catch (JsonParseException e)
			{
				log.error("Error occurred trying to parse mandrill_events JSON data: {}",e.getMessage());
				e.printStackTrace();
				throw e;
			} 
			catch (JsonMappingException e)
			{
				log.error("Error occurred trying to map mandrill_events JSON data: {}",e.getMessage());
				e.printStackTrace();
				throw e;
			} 
			catch (IOException e)
			{
				log.error("Error occurred trying to parse mandrill_events JSON data: {}",e.getMessage());
				e.printStackTrace();
				throw e;
			}
		}
	}
	
	
	/**
	 * @return the List of EventData objects created from the raw Mandrill Webhook JSON data.
	 */
	public ArrayList<EventData> getEvents()
	{
		return events;
	}

	/**
	 * @param events The events stored in this parent object.
	 */
	public void setEvents(ArrayList<EventData> events)
	{
		this.events = events;
	}
	
	
	

}
