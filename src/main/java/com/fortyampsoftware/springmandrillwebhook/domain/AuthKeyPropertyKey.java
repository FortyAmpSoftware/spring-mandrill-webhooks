/**
 * Copyright ;c) 2013  Forty Amp Software <http://www.40amps.com>
 *
 * All rights reserved. No warranty, explicit or implicit, provided.
 */
package com.fortyampsoftware.springmandrillwebhook.domain;

/**
 * @author D. Scott Felblinger
 *
 */
public class AuthKeyPropertyKey
{
	public static final String MANDRILL_AUTH_KEY_SEND = "mandrillAuthKeySend";
	public static final String MANDRILL_AUTH_KEY_HARD_BOUNCE = "mandrillAuthKeyHardBounce";
	public static final String MANDRILL_AUTH_KEY_OPENED = "mandrillAuthKeyOpened";
	public static final String MANDRILL_AUTH_KEY_SPAM = "mandrillAuthKeySpam";
	public static final String MANDRILL_AUTH_KEY_REJECTED = "mandrillAuthKeyRejected";
	public static final String MANDRILL_AUTH_KEY_DELAYED = "mandrillAuthKeyDelayed";
	public static final String MANDRILL_AUTH_KEY_SOFT_BOUNCE = "mandrillAuthKeySoftBounce";
	public static final String MANDRILL_AUTH_KEY_CLICK = "mandrillAuthKeyClick";
	public static final String MANDRILL_AUTH_KEY_UNSUBSCRIBE = "mandrillAuthKeyUnsubscribe";
	public static final String MANDRILL_AUTH_KEY_MULTI = "mandrillAuthKeyMulti";
}
