/**
 * Copyright ;c) 2013  Forty Amp Software <http://www.40amps.com>
 *
 * All rights reserved. No warranty, explicit or implicit, provided.
 */
package com.fortyampsoftware.springmandrillwebhook.domain;

/**
 * @author D. Scott Felblinger
 *
 */
public class WebhookUrlPropertyKey
{
	public static final String MANDRILL_WEBHOOK_URL_SEND = "webhookUrlSend";
	public static final String MANDRILL_WEBHOOK_URL_HARD_BOUNCE = "webhookUrlHardBounce";
	public static final String MANDRILL_WEBHOOK_URL_OPENED ="webhookUrlOpened";
	public static final String MANDRILL_WEBHOOK_URL_SPAM = "webhookUrlSpam";
	public static final String MANDRILL_WEBHOOK_URL_REJECTED = "webhookUrlRejected";
	public static final String MANDRILL_WEBHOOK_URL_DELAYED = "webhookUrlDelayed";
	public static final String MANDRILL_WEBHOOK_URL_SOFT_BOUNCE = "webhookUrlSoftBounce";
	public static final String MANDRILL_WEBHOOK_URL_CLICK = "webhookUrlClick";
	public static final String MANDRILL_WEBHOOK_URL_UNSUBSCRIBE = "webhookUrlUnsubscribe";
	public static final String MANDRILL_WEBHOOK_URL_MULTI = "webhookUrlMulti";
}
