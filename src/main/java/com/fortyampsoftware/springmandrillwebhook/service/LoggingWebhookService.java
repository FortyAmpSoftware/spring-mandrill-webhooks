/**
 * Copyright (c) 2013  Forty Amp Software <http://www.40amps.com>
 *
 *  This file is part of the Spring-Mandrill-Webhooks project:
 * (https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/overview)
 *
 *  Spring-Mandrill-Webhooks is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Spring-Mandrill-Webhooks is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with Spring-Mandrill-Webhooks.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.fortyampsoftware.springmandrillwebhook.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events;

/**
 * Reference implementation of a MandrillWebhookService used to demonstrate simple logging 
 * of Mandrill Webhook Events.
 * @author D. Scott Felblinger
 *
 */
public class LoggingWebhookService implements MandrillWebhookService
{
	private static final Logger log = LoggerFactory.getLogger(LoggingWebhookService.class);
	/* (non-Javadoc)
	 * @see com.fortyampsoftware.springmandrillwebhook.service.WebhookService#processSendEvents(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events)
	 */
	@Override
	public void processSendEvents(Events events)
	{
		log.info("processingSendEvents.  count: {}",events.getEvents().size());
		
	}

	/* (non-Javadoc)
	 * @see com.fortyampsoftware.springmandrillwebhook.service.WebhookService#processHardBounceEvents(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events)
	 */
	@Override
	public void processHardBounceEvents(Events events)
	{
		log.info("processingHardBouncEvents");
	}

	/* (non-Javadoc)
	 * @see com.fortyampsoftware.springmandrillwebhook.service.WebhookService#processOpenedEvents(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events)
	 */
	@Override
	public void processOpenedEvents(Events events)
	{
		log.info("processingOpenedEvents");
	}

	/* (non-Javadoc)
	 * @see com.fortyampsoftware.springmandrillwebhook.service.WebhookService#processSpamEvents(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events)
	 */
	@Override
	public void processSpamEvents(Events events)
	{
		log.info("processingSpamEvents");
	}

	/* (non-Javadoc)
	 * @see com.fortyampsoftware.springmandrillwebhook.service.WebhookService#processRejectedEvents(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events)
	 */
	@Override
	public void processRejectedEvents(Events events)
	{
		log.info("processingRejectedEvents");
	}

	/* (non-Javadoc)
	 * @see com.fortyampsoftware.springmandrillwebhook.service.WebhookService#processDelayedEvents(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events)
	 */
	@Override
	public void processDelayedEvents(Events events)
	{
		log.info("processingDelayedEvents");
	}

	/* (non-Javadoc)
	 * @see com.fortyampsoftware.springmandrillwebhook.service.WebhookService#processSoftBounceEvents(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events)
	 */
	@Override
	public void processSoftBounceEvents(Events events)
	{
		log.info("processingSoftBounceEvents");
	}

	/* (non-Javadoc)
	 * @see com.fortyampsoftware.springmandrillwebhook.service.WebhookService#processClickEvents(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events)
	 */
	@Override
	public void processClickEvents(Events events)
	{
		log.info("processingClickEvents");
	}

	/* (non-Javadoc)
	 * @see com.fortyampsoftware.springmandrillwebhook.service.WebhookService#processUnsubscribeEvents(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events)
	 */
	@Override
	public void processUnsubscribeEvents(Events events)
	{
		log.info("processingUnsubscribeEvents");
	}

}
