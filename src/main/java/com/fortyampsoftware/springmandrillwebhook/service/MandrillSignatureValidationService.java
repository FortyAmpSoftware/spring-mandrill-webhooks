/**
 * Copyright (c) 2013  Forty Amp Software <http://www.40amps.com>
 *
 *  This file is part of the Spring-Mandrill-Webhooks project:
 * (https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/overview)
 *
 *  Spring-Mandrill-Webhooks is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Spring-Mandrill-Webhooks is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with Spring-Mandrill-Webhooks.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.fortyampsoftware.springmandrillwebhook.service;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.TreeMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;

/**
 * MandrillSignatureValidationService is used to validate that a given Mandrill Webhook request is
 * valid/authorized and OK to be processed by the system.  The process required for authenticating
 * a Mandrill Webhook request is described here: 
 * <a href="http://help.mandrill.com/entries/23704122-Authenticating-webhook-requests" target="_new">http://help.mandrill.com/entries/23704122-Authenticating-webhook-requests</a>
 * <br />
 * This class uses the spring-security-core library to handle Base64 encoding of the HMAC-SHA1 signature.
 * 
 * @author D. Scott Felblinger
 */
@Service
public class MandrillSignatureValidationService
{
	private static final Logger log = LoggerFactory.getLogger(MandrillSignatureValidationService.class);
	
	private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";
	private static final String CHARSET_UTF_8 = "UTF-8";
	
	/**
	 * Verifies signature data for a given Mandrill Webhook request matches the signature generated
	 * by the internally stored authentication key and webhook url.   
	 * @param request Mandrill Webhook request data.
	 * @param authenticationKey stored internally used for validating a signature
	 * @param webhookUrl stored internally used for validating a signature.
	 * @return true if the signature is valid, false if not.
	 */
	public boolean verifySignature(HttpServletRequest request, String authenticationKey, String webhookUrl)
	{
		StringBuilder signatureData = new StringBuilder();
		//url
		signatureData.append(webhookUrl);
		
		//sorted post variables
		TreeMap<String, String[]> postVarMap = new TreeMap<String, String[]>(request.getParameterMap());
		for(String key : postVarMap.keySet())
		{
			signatureData.append(key);
			String [] valueArray = postVarMap.get(key);
			for(int i=0;i < valueArray.length;i++)
			{
				signatureData.append(valueArray[i]);
			}
		}
		String hmacSignature = createRFC2104HMACSignature(authenticationKey,signatureData.toString());
		String mandrillSignature = request.getHeader("X-Mandrill-Signature");
		
		log.debug("createdSig:     {}",hmacSignature);
		log.debug("X-Mandrill-Sig: {}",mandrillSignature);
		
		return hmacSignature.equals(mandrillSignature);
	}
	
	
	/**
	 * Creates a RFC2104 HMAC Signature out of passed data for a given authorization key.  
	 * Code derived from: http://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/AuthJavaSampleHMACSignature.html
	 * @param key Mandrill authorization key
	 * @param data Signature data created from the Mandrill Webhook request data.
	 * @return a valid binary HMAC-SHA1 signature that has been Base64 encoded. 
	 */
	private static String createRFC2104HMACSignature(String key, String data)
	{
		String result = null;
		try
		{
			// get an hmac_sha1 key from the raw key bytes
			SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(Charset.forName(CHARSET_UTF_8)), HMAC_SHA1_ALGORITHM);
			// get an hmac_sha1 Mac instance and initialize with the signing key
			Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
			mac.init(signingKey);

			// compute the hmac on input data bytes
			byte[] rawHmac = mac.doFinal(data.getBytes(Charset.forName(CHARSET_UTF_8)));
			result = new String(Base64.encode(rawHmac),CHARSET_UTF_8);
		}
		catch(NoSuchAlgorithmException nsae)
		{
			log.error("Unable to create HMACSignature: {}",nsae.getMessage());
			nsae.printStackTrace();
		} 
		catch (InvalidKeyException ike)
		{
			log.error("Unable to create HMACSignature: {}",ike.getMessage());
			ike.printStackTrace();
		} 
		catch (UnsupportedEncodingException uee)
		{
			log.error("Unable to create HMACSignature: {}",uee.getMessage());
			uee.printStackTrace();
		}
		return result;
	}
}
