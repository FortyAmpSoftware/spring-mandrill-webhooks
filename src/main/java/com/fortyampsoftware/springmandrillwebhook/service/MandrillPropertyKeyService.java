package com.fortyampsoftware.springmandrillwebhook.service;

import javax.annotation.Resource;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: ballmw
 * Date: 11/18/13
 * Time: 10:11 AM
 * To change this template use File | Settings | File Templates.
 */
public class MandrillPropertyKeyService implements MandrillKeyService{

    @Resource(name="mandrillWebhookProperties")
    private Properties properties;

    @Override
    public String getProperty(String key) {
        return properties.getProperty(key);
    }
}
