/**
 * Copyright (c) 2013  Forty Amp Software <http://www.40amps.com>
 *
 *  This file is part of the Spring-Mandrill-Webhooks project:
 * (https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/overview)
 *
 *  Spring-Mandrill-Webhooks is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Spring-Mandrill-Webhooks is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with Spring-Mandrill-Webhooks.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.fortyampsoftware.springmandrillwebhook.controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fortyampsoftware.springmandrillwebhook.model.mandrill.EventData;
import com.fortyampsoftware.springmandrillwebhook.model.mandrill.EventData.Event;
import com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events;
import com.fortyampsoftware.springmandrillwebhook.service.MandrillWebhookService;


/**
 * MandrillWebhookController is the primary controller used to receive and route incoming Mandrill
 * Webhook callbacks in POST form.  Each request mapping has been created to handle a specific event type as
 * defined in Mandrill's webhook documentation: <a href="http://help.mandrill.com/entries/21738186-Introduction-to-Webhooks" target="_new">http://help.mandrill.com/entries/21738186-Introduction-to-Webhooks</a>
 * 
 * @author D. Scott Felblinger
 */
@Controller
@RequestMapping("/")
public class MandrillWebhookController
{
	private static final Logger log = LoggerFactory.getLogger(MandrillWebhookController.class);
	
	/**
	 * MandrillWebhookService responsible for processing in-bound events.  
	 */
	@Autowired
	private MandrillWebhookService mandrillWebhookService;

	/**
	 * Catch-all mapping to handle HEAD requests to this controller
	 * @param request
	 * @return HttpStatus.OK
	 */
    @RequestMapping(value={"/multi","send","hard-bounce","opened","spam","rejected","delayed","soft-bounce","click","unsubscribe"},method=RequestMethod.HEAD)
	public ResponseEntity<String>processAnyHeadRequest(HttpServletRequest request)
	{
		log.debug("Recieved HEAD request for {}"+request.getContextPath()+request.getServletPath());
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	/**
	 * A catch-all method that will process any Mandrill Webhook callback events sent to it.  It will
	 * break apart individual events, batch them internally then route them to the appropriate
	 * MandrillWebhookService method for processing.  This method will be utilized if someone
	 * wants to only utilize one Webhook URL for handling all callbacks.
	 * 
	 * @param events
	 * @param request
	 * @return HttpStatus.OK
	 */
	@RequestMapping(value="/multi",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String>processAnyRequest(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request)
	{
		HashMap<Event,ArrayList<EventData>> eventContainer = new HashMap<Event,ArrayList<EventData>>();
		
		//fill up the container
		for(EventData eventData : events.getEvents())
		{
			switch(eventData.getEvent())
			{
				case SEND:
					fillContainer(eventContainer,Event.SEND,eventData);
					break;
				case HARD_BOUNCE:
					fillContainer(eventContainer,Event.HARD_BOUNCE,eventData);
					break;
				case OPEN:
					fillContainer(eventContainer,Event.OPEN,eventData);
					break;
				case SPAM:
					fillContainer(eventContainer,Event.SPAM,eventData);
					break;
				case REJECT:
					fillContainer(eventContainer,Event.REJECT,eventData);
					break;
				case SOFT_BOUNCE:
					fillContainer(eventContainer,Event.SOFT_BOUNCE,eventData);
					break;
				case CLICK:
					fillContainer(eventContainer,Event.CLICK,eventData);
					break;
				case UNSUB:
					fillContainer(eventContainer,Event.UNSUB,eventData);
					break;
				case DEFERRAL:
					fillContainer(eventContainer,Event.DEFERRAL,eventData);
					break;
				default:
					break;
			}
		}
		
		//empty out the container
		Set<Event> containerKeys = eventContainer.keySet();
		for(Event key : containerKeys)
		{
			switch(key)
			{
				case SEND:
					mandrillWebhookService.processSendEvents(createNewEvents(eventContainer,key));
					break;
				case HARD_BOUNCE:
					mandrillWebhookService.processHardBounceEvents(createNewEvents(eventContainer,key));
					break;
				case OPEN:
					mandrillWebhookService.processOpenedEvents(createNewEvents(eventContainer,key));
					break;
				case SPAM:
					mandrillWebhookService.processSpamEvents(createNewEvents(eventContainer,key));
					break;
				case REJECT:
					mandrillWebhookService.processRejectedEvents(createNewEvents(eventContainer,key));
					break;
				case SOFT_BOUNCE:
					mandrillWebhookService.processSoftBounceEvents(createNewEvents(eventContainer,key));
					break;
				case CLICK:
					mandrillWebhookService.processClickEvents(createNewEvents(eventContainer,key));
					break;
				case UNSUB:
					mandrillWebhookService.processUnsubscribeEvents(createNewEvents(eventContainer,key));
					break;
				case DEFERRAL:
					mandrillWebhookService.processDelayedEvents(createNewEvents(eventContainer,key));
					break;
				default:
					break;
			}
		}
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	/**
	 * Helper method to processAnyRequest().  Creates a new set of Events for a given key. 
	 * @param eventContainer
	 * @param key
	 * @return Events containing EventData for the given key.
	 */
	private Events createNewEvents(HashMap<Event,ArrayList<EventData>> eventContainer, Event key)
	{
		try
		{
			Events events = new Events(null);
			events.setEvents(eventContainer.get(key));
			return events;
		}
		catch(IOException ie)
		{
			return null;
		}
	}
	
	/**
	 * Helper method to processAnyRequest().  Creates an array list to contain EventData
	 * for a given key if it does not already exist, otherwise it fills an existing list
	 * with the EventData.
	 * 
	 * @param map
	 * @param key
	 * @param eventData
	 */
	private void fillContainer(HashMap<Event,ArrayList<EventData>> map, Event key, EventData eventData)
	{
		if(!map.containsKey(key))
		{
			ArrayList<EventData> dataList = new ArrayList<EventData>();
			dataList.add(eventData);
			map.put(key, dataList);
		}
		else
		{
			ArrayList<EventData> data = map.get(key);
			data.add(eventData);
		}
	}

	/**
	 * Handles the Mandrill Sent Webhook callback.
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if successfully processed.
	 */
	@RequestMapping(value="/send",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processSend(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		mandrillWebhookService.processSendEvents(events);
		return new ResponseEntity<String>(HttpStatus.OK);
	}

	/**
	 * Handles the Mandrill Hard Bounce Webhook callback.
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if successfully processed.
	 */
	@RequestMapping(value="/hardbounce",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processHardBounce(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		mandrillWebhookService.processHardBounceEvents(events);
		return new ResponseEntity<String>(HttpStatus.OK);
	}

	/**
	 * Handles the Mandrill Opened Webhook callback.
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if successfully processed.
	 */
	@RequestMapping(value="/opened",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processOpened(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		mandrillWebhookService.processOpenedEvents(events);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	/**
	 * Handles the Mandrill Spam Webhook callback.
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if successfully processed.
	 */	
	@RequestMapping(value="/spam",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processSpam(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		mandrillWebhookService.processSpamEvents(events);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	
	/**
	 * Handles the Mandrill Rejected Webhook callback.
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if successfully processed.
	 */
	@RequestMapping(value="/rejected",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processRejected(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		mandrillWebhookService.processRejectedEvents(events);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	
	/**
	 * Handles the Mandrill Delayed Webhook callback.
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if successfully processed.
	 */
	@RequestMapping(value="/delayed",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processDelayed(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		mandrillWebhookService.processDelayedEvents(events);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	
	/**
	 * Handles the Mandrill Soft Bounce Webhook callback.
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if successfully processed.
	 */
	@RequestMapping(value="/soft-bounce",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processSoftBounce(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		mandrillWebhookService.processSoftBounceEvents(events);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	/**
	 * Handles the Mandrill Click Webhook callback.
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if successfully processed.
	 */
	@RequestMapping(value="/click",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processClick(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		mandrillWebhookService.processClickEvents(events);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	/**
	 * Handles the Mandrill Unsubscribe Webhook callback.
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if successfully processed.
	 */
	@RequestMapping(value="/unsubscribe",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processUnsubscribe(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		mandrillWebhookService.processUnsubscribeEvents(events);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
}
