/**
 * Copyright (c) 2013  Forty Amp Software <http://www.40amps.com>
 *
 *  This file is part of the Spring-Mandrill-Webhooks project:
 * (https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/overview)
 *
 *  Spring-Mandrill-Webhooks is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Spring-Mandrill-Webhooks is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with Spring-Mandrill-Webhooks.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.fortyampsoftware.springmandrillwebhook.controller;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;

import com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events;
import com.fortyampsoftware.springmandrillwebhook.service.MandrillWebhookService;

/**
 * @author D. Scott Felblinger
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class MandrillWebhookControllerTest
{

	private MockHttpServletRequest request;
	private Events events;
	@Mock
	private MandrillWebhookService mandrillWebhookService;
	
	@InjectMocks
	private MandrillWebhookController mandrillWebhookController;
	
	private static final Logger log = LoggerFactory.getLogger(MandrillWebhookControllerTest.class);
	
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		mandrillWebhookController = new MandrillWebhookController();
		MockitoAnnotations.initMocks(this);
		request = new MockHttpServletRequest();
	}

	

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.MandrillWebhookController#processAnyRequest(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 * @throws URISyntaxException 
	 * @throws IOException 
	 */
	@Test
	public void testProcessAnyRequest() throws IOException
	{
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String multiFileContents = createStringFromFile("mandrill-request-multi.json");
		events = new Events(multiFileContents);
		ResponseEntity<String> response = mandrillWebhookController.processAnyRequest(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}
	
	
	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.MandrillWebhookController#processSend(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 * @throws URISyntaxException 
	 * @throws IOException 
	 */
	@Test
	public void testProcessSend() throws IOException
	{
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-send.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = mandrillWebhookController.processSend(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.MandrillWebhookController#processHardBounce(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessHardBounce() throws IOException
	{
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-hard-bounce.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = mandrillWebhookController.processHardBounce(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.MandrillWebhookController#processOpened(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessOpened() throws IOException
	{
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-open.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = mandrillWebhookController.processOpened(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.MandrillWebhookController#processSpam(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessSpam() throws IOException
	{
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-spam.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = mandrillWebhookController.processSpam(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.MandrillWebhookController#processRejected(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessRejected() throws IOException
	{
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-reject.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = mandrillWebhookController.processRejected(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.MandrillWebhookController#processDelayed(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessDelayed() throws IOException
	{
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-deferral.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = mandrillWebhookController.processDelayed(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.MandrillWebhookController#processSoftBounce(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessSoftBounce() throws IOException
	{
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-soft-bounce.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = mandrillWebhookController.processSoftBounce(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.MandrillWebhookController#processClick(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessClick() throws IOException
	{
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-click.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = mandrillWebhookController.processClick(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.MandrillWebhookController#processUnsubscribe(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessUnsubscribe() throws IOException
	{
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-unsubscribe.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = mandrillWebhookController.processUnsubscribe(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}
	
	
	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.MandrillWebhookController#processAnyHeadRequest(javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessHeadOk()
	{
		ResponseEntity<String> response = mandrillWebhookController.processAnyHeadRequest(request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}
	
	
	/**
	 * Helper method that loads a file containing test json requests from mandrill
	 * and returns the contents as a string.
	 * 
	 * @param fileName
	 * @return String data of the JSON test file
	 */
	private String createStringFromFile(String fileName)
	{
		String sendFileContents = null;
		try
		{
			URL responseFileUrl = this.getClass().getClassLoader().getResource(fileName);
			File testJsonFile = new File(responseFileUrl.toURI());
			sendFileContents = FileUtils.readFileToString(testJsonFile);
		} 
		catch (IOException e)
		{
			log.error("Exception trying to load test file: {}.  Check test configuration",fileName);
			e.printStackTrace();
			fail("Test failed due to improper test setup");
		} 
		catch (URISyntaxException e)
		{
			log.error("Bad URI trying to load test file: {}.  Check test configuration",fileName);
			e.printStackTrace();
			fail("Test failed due to improper test setup");
		}
		
		return sendFileContents;
	}

}
