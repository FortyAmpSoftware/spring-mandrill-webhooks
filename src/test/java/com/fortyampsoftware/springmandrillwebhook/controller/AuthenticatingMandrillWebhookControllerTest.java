/**
 * Copyright (c) 2013  Forty Amp Software <http://www.40amps.com>
 *
 *  This file is part of the Spring-Mandrill-Webhooks project:
 * (https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/overview)
 *
 *  Spring-Mandrill-Webhooks is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Spring-Mandrill-Webhooks is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with Spring-Mandrill-Webhooks.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.fortyampsoftware.springmandrillwebhook.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.fortyampsoftware.springmandrillwebhook.service.MandrillKeyService;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;

import com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events;
import com.fortyampsoftware.springmandrillwebhook.service.MandrillSignatureValidationService;
import com.fortyampsoftware.springmandrillwebhook.service.MandrillWebhookService;

/**
 * @author D. Scott Felblinger
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthenticatingMandrillWebhookControllerTest
{
	private static final Logger log = LoggerFactory.getLogger(AuthenticatingMandrillWebhookControllerTest.class);
	
	private MockHttpServletRequest request;
	private Events events;
	@Mock
	private MandrillWebhookService mandrillWebhookService;
	
	@Mock
	private MandrillSignatureValidationService mandrillSignatureValidationService;
	
	@InjectMocks
	private AuthenticatingMandrillWebhookController authenticatingMandrillWebhookController;
	
	@Mock
	private MandrillKeyService mandrillKeyService;
	
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		authenticatingMandrillWebhookController = new AuthenticatingMandrillWebhookController();
		MockitoAnnotations.initMocks(this);
		request = new MockHttpServletRequest();
		request.setContextPath("/JUNIT-TEST");
		request.setServletPath("/IGNORE THIS WARNING");
	}

	
	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processAnyRequest(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessAnyRequestOk() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(true);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String multiFileContents = createStringFromFile("mandrill-request-multi.json");
		events = new Events(multiFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processAnyRequest(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}
	
	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processAnyRequest(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessAnyRequestUnauthorized() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(false);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String multiFileContents = createStringFromFile("mandrill-request-multi.json");
		events = new Events(multiFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processAnyRequest(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.UNAUTHORIZED));
	}
	
	
	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processSend(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessSendOk() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(true);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-send.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processSend(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}
	
	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processSend(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessSendUnauthorized() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(false);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-send.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processSend(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.UNAUTHORIZED));
	}	
	
	

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processHardBounce(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessHardBounceOk() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(true);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-hard-bounce.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processHardBounce(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}
	
	
	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processHardBounce(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessHardBounceUnauthorized() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(false);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-hard-bounce.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processHardBounce(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.UNAUTHORIZED));
	}

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processOpened(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessOpenedOk() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(true);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-open.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processOpened(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}
	
	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processOpened(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessOpenedUnauthorized() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(false);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-open.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processOpened(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.UNAUTHORIZED));
	}

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processSpam(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessSpamOk() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(true);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-spam.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processSpam(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}
	
	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processSpam(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessSpamUnauthorized() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(false);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-spam.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processSpam(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.UNAUTHORIZED));
	}
	

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processRejected(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessRejectedOk() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(true);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-reject.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processRejected(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}
	
	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processRejected(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessRejectedUnauthorized() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(false);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-reject.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processRejected(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.UNAUTHORIZED));
	}

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processDelayed(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessDelayedOk() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(true);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-deferral.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processDelayed(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processDelayed(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessDelayedUnauthorized() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(false);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-deferral.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processDelayed(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.UNAUTHORIZED));
	}
	
	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processSoftBounce(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessSoftBounceOk() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(true);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-soft-bounce.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processSoftBounce(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}
	
	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processSoftBounce(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessSoftBounceUnauthorized() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(false);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-soft-bounce.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processSoftBounce(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.UNAUTHORIZED));
	}

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processClick(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessClickOk() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(true);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-click.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processClick(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}
	
	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processClick(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessClickUnauthorized() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(false);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-click.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processClick(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.UNAUTHORIZED));
	}

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processUnsubscribe(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessUnsubscribeOk() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(true);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-unsubscribe.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processUnsubscribe(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}


	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processUnsubscribe(com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessUnsubscribeUnauthorized() throws IOException
	{
		when(mandrillSignatureValidationService.verifySignature(isA(HttpServletRequest.class), anyString(), anyString())).thenReturn(false);
		
		//Get a test mandrill json response off the project classpath (resource folder) for testing
		String sendFileContents = createStringFromFile("mandrill-request-unsubscribe.json");
		events = new Events(sendFileContents);
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processUnsubscribe(events, request);
		assertTrue(response.getStatusCode().equals(HttpStatus.UNAUTHORIZED));
	}
	
	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.controller.AuthenticatingMandrillWebhookController#processAnyHeadRequest(javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testProcessHeadOk()
	{
		ResponseEntity<String> response = authenticatingMandrillWebhookController.processAnyHeadRequest(request);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}



	/**
	 * Helper method that loads a file containing test json requests from mandrill
	 * and returns the contents as a string.
	 * 
	 * @param fileName
	 * @return String data of the JSON test file
	 */
	private String createStringFromFile(String fileName)
	{
		String sendFileContents = null;
		try
		{
			URL responseFileUrl = this.getClass().getClassLoader().getResource(fileName);
			File testJsonFile = new File(responseFileUrl.toURI());
			sendFileContents = FileUtils.readFileToString(testJsonFile);
		} 
		catch (IOException e)
		{
			log.error("Exception trying to load test file: {}.  Check test configuration",fileName);
			e.printStackTrace();
			fail("Test failed due to improper test setup");
		} 
		catch (URISyntaxException e)
		{
			log.error("Bad URI trying to load test file: {}.  Check test configuration",fileName);
			e.printStackTrace();
			fail("Test failed due to improper test setup");
		}
		
		return sendFileContents;
	}
	
}
