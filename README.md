# README File for Spring-Mandrill-Webhooks

# Background
This library was designed to be a pluggable library of spring controllers and services that quickly 
let you setup and process Mandrill Webhooks in your application. The project itself is a spring mvc 3.1 
reference application that can actually be run for testing/learning purposes. The true use of this library 
however is to utilize the latest spring-mandrill-webhook-x.x.x-RELEASE.jar file in your own Spring MVC 
project and simply extend one of the pre-built MandrillWebhookControllers and implement your own 
MandrillWebhookService. The inner workings of the library handle the mapping of Mandrill's JSON callbacks 
into usable Java model objects.

# Getting Started
Read the wiki at [https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/wiki/Home](https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/wiki/Home) 
for documentation on using the library or running the reference implementation.  The latest
Javadoc can be found here: [https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/src/bcaff6ac85c420909461a293f803e6d0e0a4e06b/javadoc?at=master](https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/src/bcaff6ac85c420909461a293f803e6d0e0a4e06b/javadoc?at=master)


# Legal:
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser Public License for more details.

    You should have received a copy of the GNU Lesser Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
